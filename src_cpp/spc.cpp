/* Records SPC into wave file. Uses dsp_filter to give more authentic sound.

Usage: play_spc [test.spc]
*/

#ifdef EMSCRIPTEN

#include <emscripten/bind.h>

using namespace emscripten;

#endif

#include "snes_spc/spc.h"
#include "wave_writer.h"

#include <string>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>

struct spc_state_t {
  SNES_SPC* snes_spc;
  SPC_Filter* filter;
};

static struct spc_state_t state;

unsigned char* load_file( const char* path, long* size_out )
{
	size_t size;
	unsigned char* data;
	
	FILE* in = fopen( path, "rb" );
	if ( !in ) return NULL;
	
	fseek( in, 0, SEEK_END );
	size = ftell( in );
	if ( size_out )
		*size_out = size;
	rewind( in );
	
	data = (unsigned char*) malloc( size );
	if ( !data ) return NULL; //error( "Out of memory" );
	
	if ( fread( data, 1, size, in ) < size ) return NULL; //error( "Couldn't read file" );
	fclose( in );
	
	return data;
}

int get_dsp_register_spc(int reg_num) {
  SNES_SPC* snes_spc = state.snes_spc;
  if (reg_num >= 0x00 && reg_num <= 0x7F) {
    return snes_spc->dsp.m.regs[reg_num];
  }
  else {
    return 0;
  }
}

val get_voice_spc(int i) {
  SNES_SPC* snes_spc = state.snes_spc;
  if (i >= 0 && i <= 7) {
    struct SPC_DSP::voice_t* v = &snes_spc->dsp.m.voices[i];
    val a = val::object();
    a.set("env", val(v->env));

    return a;
  }
  else {
    return val::object();
  }
}

  // struct voice_t
  // {
  //   int buf [brr_buf_size*2];// decoded samples (twice the size to simplify wrap handling)
  //   int* buf_pos;           // place in buffer where next samples will be decoded
  //   int interp_pos;         // relative fractional position in sample (0x1000 = 1.0)
  //   int brr_addr;           // address of current BRR block
  //   int brr_offset;         // current decoding offset in BRR block
  //   int kon_delay;          // KON delay/current setup phase
  //   env_mode_t env_mode;
  //   int env;                // current envelope level
  //   int hidden_env;         // used by GAIN mode 7, very obscure quirk
  //   int volume [2];         // copy of volume from DSP registers, with surround disabled
  //   int enabled;            // -1 if enabled, 0 if muted
  // };

void init_spc() {
	SNES_SPC* snes_spc = spc_new();
	SPC_Filter* filter = spc_filter_new();
  if (!snes_spc || !filter) {
  	return;
  }
  state.snes_spc = snes_spc;
  state.filter = filter;
}

void load_spc(const std::string path) {
  SNES_SPC* snes_spc = state.snes_spc;
  SPC_Filter* filter = state.filter;
  /* Load file into memory */
  long spc_size;
  void* spc = load_file(path.c_str(), &spc_size);
  spc_load_spc( snes_spc, spc, spc_size );
  free(spc);
  spc_clear_echo(snes_spc);
  spc_filter_clear(filter);
}

void play_spc(int16_t* buf, size_t buf_size) {
	SNES_SPC* snes_spc = state.snes_spc;
	SPC_Filter* filter = state.filter;
	spc_play(snes_spc, buf_size, buf);
	spc_filter_run(filter, buf, buf_size);
}
#ifdef EMSCRIPTEN 

#define LARGEST_BUF_SIZE 32768

// void play_spc_js(val e) {
// 	val buf1 = e["outputBuffer"].call<val>("getChannelData", 0);
// 	uint32_t buf_size = buf1["length"].as<unsigned int>();

//   printf("Got buffer, with size %d\n", buf_size);

//   int16_t buf[LARGEST_BUF_SIZE];
// 	SNES_SPC* snes_spc = state.snes_spc;
// 	SPC_Filter* filter = state.filter;
// 	spc_play(snes_spc, buf_size, buf);
// 	spc_filter_run(filter, buf, buf_size);
// 	for (uint32_t i = 0; i < buf_size; i++) {
//     buf1[val(i)] = val((float)buf[i] / 32767.0f);
// 	}
// }

val play_spc_js(size_t buf_size) {

	size_t ren_size = buf_size * 2;

  // printf("Got buffer, with size %d\n", buf_size);

  int16_t buf[LARGEST_BUF_SIZE];
	SNES_SPC* snes_spc = state.snes_spc;
	SPC_Filter* filter = state.filter;
	spc_play(snes_spc, ren_size, buf);
	spc_filter_run(filter, buf, ren_size);
	val buf1 = val::array();
	for (uint32_t i = 0; i < (ren_size); i++) {
    buf1.call<void>("push", val((float)buf[i] / 32767.0f));
    // bufL.call<void>("push", val((float)buf[i] / 32767.0f));
    // bufR.call<void>("push", val((float)buf[i+buf_size] / 32767.0f));
	}
	// buf1.call<void>("push",bufL);
	// buf1.call<void>("push",bufR);
	return buf1;
}

#endif

void cleanup_spc() {
	SNES_SPC* snes_spc = state.snes_spc;
	SPC_Filter* filter = state.filter;
	spc_filter_delete( filter );
	spc_delete( snes_spc );
	state.snes_spc = NULL;
	state.filter = NULL;
}

#ifdef EMSCRIPTEN

EMSCRIPTEN_BINDINGS(my_module) {
    function("init_spc", &init_spc);
    function("load_spc", &load_spc);
    function("play_spc", &play_spc_js);
    function("cleanup_spc", &cleanup_spc);
    function("get_dsp_register_spc", &get_dsp_register_spc);
    function("get_voice_spc", &get_voice_spc);
}

#else

int main( int argc, char** argv )
{
	/* Create emulator and filter */
	init_spc("test.spc");
	/* Record 20 seconds to wave file */
	wave_open( spc_sample_rate, "out.wav" );
	wave_enable_stereo();
	while ( wave_sample_count() < 20 * spc_sample_rate * 2 )
	{
		/* Play into buffer */
		#define BUF_SIZE 2048
		short buf [BUF_SIZE];
		play_spc(buf, BUF_SIZE);
		wave_write( buf, BUF_SIZE );
	}
	
	cleanup_spc();
	wave_close();
	
	return 0;
}

#endif
