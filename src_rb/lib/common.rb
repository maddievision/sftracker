def after time, &block
  `setTimeout(#{block}, #{time})`
end

def each_frame &block
  f = lambda do |timestamp|
    res = block.call timestamp
    `window.requestAnimationFrame(#{f})` unless res
  end
  `window.requestAnimationFrame(#{f})`
end