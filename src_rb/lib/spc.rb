class SPC
  def initialize
    `Module.init_spc()`
  end
  def load fn
    `Module.load_spc(#{fn})`
  end
  def load_data buffer
    shiznit = lambda do
      load "/test.spc"
    end

    %x{

      try {
        FS.unlink("/test.spc");
      }
      catch (e) {
        
      }
      FS.createDataFile("/", "test.spc", new Uint8Array(#{buffer}), true, true, true);
      #{shiznit}();

    }
  end
  def load_file file
    shoz = lambda do |x|
      load_data x
    end
    %x{
      var reader = new FileReader();
      reader.onload = function(e) {
        var buffer = reader.result;
        #{shoz}(buffer);
      }
      reader.readAsArrayBuffer(#{file});
    }
  end
  def get_dsp_reg addr
    `Module.get_dsp_register_spc(#{addr})`
  end
  def get_play_buffer size
    `Module.play_spc(#{size})`
  end
  def get_voice i
    `Module.get_voice_spc(#{i})`
  end

  def synth
    @synth ||= begin
      fob = lambda do |i|
        v = get_voice i
        if `#{v}.env` > 0
          vl = get_dsp_reg 0x01 + (i << 4)
          vr = get_dsp_reg 0x01 + (i << 4)
          vl -= 0x100 if vl > 0x80
          vr -= 0x100 if vr > 0x80
          vh = get_dsp_reg 0x09 + (i << 4)
          vh -= 0x100 if vh > 0x80


          a = get_dsp_reg 0x02 + (i << 4)
          b = get_dsp_reg 0x03 + (i << 4)
          s = get_dsp_reg 0x04 + (i << 4)
   
          ff = ((b << 8) + a)
          degrees = 12
          midi_ref = 69
          freq_ref = 440

          vl = `Math.abs(#{vl})`
          vr = `Math.abs(#{vr})`
          vh = `Math.abs(#{vh})`
          vv = (vl + vr) / 127.0

          n = 0

          %x{
            try {
              #{n} = (Math.log(#{ff}) - Math.log(#{freq_ref})) / Math.log(2) * #{degrees} + #{midi_ref};
            }
            catch (e) {

            }
          }

          n = (n - 31) + 24

          n = 0 if n < 0
          n = 127 if n > 127

          fff = %x{
            {
              note: #{n},
              srcn: #{s},
              prs: [#{vh} / 32,#{vh} / 32],
              avol: (#{v}.env / 2000) * #{vv},
              enabled: true
            }
          }
        else
          fff = %x{
            {
              note: 0,
              srcn: 0,
              prs: [0,0],
              avol: 0,
              enabled: false              
            }
          }
        end

        fff
      end

      j = %x{
        {
          get_voice: #{fob}
        }
      }

      j

    end
  end

  # pump this shit into node.onaudioprocess
  def processor
    @processor ||= %x{
      function (e) {
        buf1 = e.outputBuffer.getChannelData(0);
        buf2 = e.outputBuffer.getChannelData(1);
        var rat = 0.7256235828;
        var buf_size = buf1.length;
        var scale_size = Math.ceil(buf_size * rat); // poor man's interpolation

        var inbuf = Module.play_spc(scale_size);

        // console.log(inbuf);

        for (var i = 0; i < (buf_size); i++) {
          // buf1[i] = inbuf[i];
          var x = Math.round(i * rat); // poor man's interpolation
          buf1[i] = inbuf[x*2];
          // buf2[i] = inbuf[i+buf_size];
          buf2[i] = inbuf[x*2 + 1];
        }
      }    
    }
  end
end