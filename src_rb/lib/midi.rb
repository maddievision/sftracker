class MIDI
  EVENT_TYPE_NAMES = [
      '0','1','2','3','4','5','6','7',
      :note_off,
      :note_on,
      :note_pressure,
      :control,
      :program,
      :pressure,
      :pitch,
      :system
  ]

  def initialize &block
    @m = nil # m = MIDIAccess object for you to make calls on

    success = lambda do |access| 

      @m = access;

      # Things you can do with the MIDIAccess object:
      @inputs = `#{@m}.inputs()` #;   // inputs = array of MIDIPorts
      @outputs = `#{@m}.outputs()`  #; // outputs = array of MIDIPorts

      block.call self

      # // var o = m.outputs()[0];           // grab first output device
      # // o.send( [ 0x90, 0x45, 0x7f ] );     // full velocity note on A4 on channel zero
      # // o.send( [ 0x80, 0x45, 0x7f ], window.performance.now() + 1000 );  // full velocity A4 note off in one second.

    end

    error = lambda { |err| puts "some shit happened" }

    `if (navigator.requestMIDIAccess != null) { navigator.requestMIDIAccess().then( #{success}, #{error} ) }`
    nil
  end

  def process &block
    midi_handler = lambda do |event|
      block.call self.class.convert_event event
    end

    @inputs.each do |input|
      `input.onmidimessage = #{midi_handler}` #; // onmidimessage( event ), event.data & event.receivedTime are populated
    end
  end

  def send output, data
    if @m && `#{@m}.outputs`
      o = `#{@m}.outputs()[#{output}]`
      %x{
        if (#{o} != null)
          {
            #{o}.send(#{data})
          }
      }
    end
  end

  def outputs
    @outputs
  end

  def self.convert_event event
    data = []
    %x{
      for (var i = 0; i < #{event}.data.length; i++) {
       #{data}.push(#{event}.data[i]);
      }
    }
    evt = { 
      type: EVENT_TYPE_NAMES[`#{event}.data[0] >> 4`],
      timestamp: `#{event}.receivedTime`,
      data: data 
    }
    evt[:type] = :note_off if evt[:type] == :note_on && data.last == 0
    evt[:channel] = (evt[:data][0] % 16) if evt[:type] != :system
    evt
  end

  def self.event_from_data data, timestamp=0
    convert_event %x{
      {
        data: #{data},
        receivedTime: #{timestamp}
      }
    }
  end

end