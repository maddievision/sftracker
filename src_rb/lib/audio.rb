class Audio
  def initialize buffer_size, channels
    @buffer_size = buffer_size
    @channels = channels
    @actx = `window.AudioContext || window.webkitAudioContext`
    if `#{@actx} == null`
      `alert("your browser is shit")`      
      return
    end

    puts "Creating audio context"

    @ctx = `new #{@actx}`

    puts "Creating script node"

    node_create = `#{@ctx}.createJavaScriptNode || #{@ctx}.createScriptProcessor`
    @node = `#{node_create}.call(#{@ctx}, #{buffer_size}, 0, #{channels})`
    @sample_rate = `#{@ctx}.sampleRate`

    @node_connected = false
  end

  def process &block
    `#{@node}.onaudioprocess = #{block}`
  end

  def connect_node
    puts "Connecting node"
    `#{@node}.connect(#{@ctx}.destination)`
    @node_connected = true
  end

  def disconnect_node
    puts "Disconnecting node"
    `#{@node}.connect(#{@ctx}.destination)`
    @node_connected = false
  end

end