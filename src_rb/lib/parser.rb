class Parser

  RX_PATTERNS = {
    "Note"             => /^([a-g])([\-#+s])?([0-9]{1,3})?(,+)?/,
    "Repeat"           => /^\\/,
    "Extend"           => /^\./,
    "Rest"             => /^\//,
    "Length"           => /^[lL]([0-9]{1,3})/,
    "Octave"           => /^o([0-9]{1,2})/,
    "Duty"             => /^p([0-9])/,
    "Gate"             => /^m([0-9]{1,2})/,
    "Tempo"            => /^t([0-9]{1,3})/,
    "UpOctave"         => /^>([0-9]{0,1})/,
    "DownOctave"       => /^<([0-9]{0,1})/,
    "UpOctave"         => /^>([0-9]{0,1})/,
    "DownRelTranspose" => /^y--([0-9]{1,2})?/,
    "UpRelTranspose"   => /^y\+\+([0-9]{1,2})?/,
    "DownTranspose"    => /^y-([0-9]{1,2})/,
    "UpTranspose"      => /^y([0-9]{1,2})/,   
    "DownDetune"       => /^x-([0-9]{1,2})/,
    "UpDetune"         => /^x([0-9]{1,2})/,
    "Include"          => /^\$([A-Za-z0-9_\/]+)/,
    "Comment"          => /^#([^\s]*)/,
    "LoopStart"        => /^\[/,
    "LoopCoda"         => /^:/,
    "LoopEnd"          => /^\]([0-9]{1,2})/,
    "MacroDef"         => /^@(\w+)\{([^\}]*)\}/,
    "MacroCall"        => /^@(\w+)/,
    "White"            => /^(\s+)/,
  }

  NOTE_MAP = {
    "c"=>0,
    "d"=>2,
    "e"=>4,
    "f"=>5,
    "g"=>7,
    "a"=>9,
    "b"=>11
  }

  attr_reader :cmds, :events, :macros

  def initialize s
    @macros = {}
    @source = s
    @cmds = parse s.gsub(/\n/," ")
    @events = parse_cmds @cmds
  end

  def try_pattern name, s, i
    res = RX_PATTERNS[name].match(s[i...(s.size)])
    return nil if res.nil?
    return [res, (res[0].size)+i]
  end

  def parse_mml s, i=0, cmds=[]
    s = s.downcase

    i = 0
    tok = nil
    
    while i < s.size
      if tok = try_pattern("Note", s, i)
        mod = 0
        if tok[0][2] == "+" || tok[0][2] == "s" || tok[0][2] == "#"
          mod = 1
        elsif tok[0][2] == "-"
          mod = -1
        end
        nlen = 0
        if tok[0][3]
          nlen = tok[0][3].to_i
        end
        lenmul = 1.0
        if tok[0][4]
          lenmul = 1.0 * (2.0 - (1.0 / (2.0 ** tok[0][4].size)))
        end
        nn = NOTE_MAP[tok[0][1]] + mod
        cmds << ["note", nn, nlen, lenmul]
      elsif tok = try_pattern("Repeat", s, i) 
        nlen = 0
        if tok[0][1]
          nlen = tok[0][1].to_i
        end
        lenmul = 1.0
        if tok[0][2]
          lenmul = 1.0 * (2.0 - (1.0 / (2.0 ** tok[0][2].size)))
        end
        cmds << ["repeat", 0, nlen, lenmul]
      elsif tok = try_pattern("Extend", s, i) 
        nlen = 0
        if tok[0][1]
          nlen = tok[0][1].to_i
        end
        lenmul = 1.0
        if tok[0][2]
          lenmul = 1.0 * (2.0 - (1.0 / (2.0 ** tok[0][2].size)))
        end
        cmds << ["extend", 0, nlen, lenmul]
      elsif tok = try_pattern("Rest", s, i) 
        nlen = 0
        if tok[0][1]
          nlen = tok[0][1].to_i
        end
        lenmul = 1.0
        if tok[0][2]
          lenmul = 1.0 * (2.0 - (1.0 / (2.0 ** tok[0][2].size)))
        end
        cmds << ["rest", 0, nlen, lenmul]
      elsif tok = try_pattern("Length", s, i) 
          cmds << ["len", tok[0][1].to_i]
      elsif tok = try_pattern("Octave", s, i) 
          cmds << ["oct", tok[0][1].to_i]
      elsif tok = try_pattern("DownOctave", s, i) 
          n = 1
          if tok[0][1].size > 0 
              n = tok[0][1].to_i
          end
          cmds << ["octdn", n]
      elsif tok = try_pattern("UpOctave", s, i) 
          n = 1
          if tok[0][1].size > 0 
              n = tok[0][1].to_i
          end
          cmds << ["octup", n]
      elsif tok = try_pattern("Duty", s, i) 
          cmds << ["duty", tok[0][1].to_i]
      elsif tok = try_pattern("DownRelTranspose", s, i) 
          n = 1
          if tok[0][1]
            n = tok[0][1].to_i
          end
          cmds << ["transreldn", n]
      elsif tok = try_pattern("UpRelTranspose", s, i) 
          n = 1
          if tok[0][1]
            n = tok[0][1].to_i
          end
          cmds << ["transrelup", n]
      elsif tok = try_pattern("DownTranspose", s, i) 
          cmds << ["transdn", tok[0][1].to_i]
      elsif tok = try_pattern("UpTranspose", s, i) 
          cmds << ["transup", tok[0][1].to_i]
      elsif tok = try_pattern("DownDetune", s, i) 
          cmds << ["tunedn", tok[0][1].to_i]
      elsif tok = try_pattern("UpDetune", s, i) 
          cmds << ["tuneup", tok[0][1].to_i]
      elsif tok = try_pattern("Gate", s, i) 
          cmds << ["gate", tok[0][1].to_i]
      elsif tok = try_pattern("Tempo", s, i) 
          cmds << ["tempo", tok[0][1].to_i]
      elsif tok = try_pattern("LoopStart", s, i) 
          cmds << ["lpstart"]
      elsif tok = try_pattern("LoopCoda", s, i) 
          cmds << ["lpcoda"]
      elsif tok = try_pattern("LoopEnd", s, i) 
          cmds << ["lpend", tok[0][1].to_i]
      elsif tok = try_pattern("Comment", s, i) 
          cmds << ["comment", tok[0][1]]
      elsif tok = try_pattern("MacroDef", s, i) 
          cmds << ["defmacro", tok[0][1], tok[0][2]]
          @macros[tok[0][1]] = tok[0][2]
      elsif tok = try_pattern("MacroCall", s, i) 
          if @macros.has_key?(tok[0][1])
              macrotext = @macros[tok[0][1]]
              cmds << ["callmacro", tok[0][1], macrotext]
              cmds += parse_mml(macrotext)
              cmds << ["endmacro", tok[0][1]]
          end
      elsif tok = try_pattern("White", s, i) 
        # ignore whitespace
      else 
          cmds << ["err", i, s[i..(i+1)]]
          return cmds
      end
    
      if tok 
        i = tok[1]
      end
    end

    cmds
  end

  def parse_cmds chans
    evs = []
    ppqn = 96 * 4
    chans.each_with_index do |cmds, chan| 
        loopStack = []
        loopI = -1
        noteIsOn = false
        curNote = -1
        curNoteStart = -1.0
        curNoteGate = 0
        curOctave = 5
        curLen = 8
        curGate = 0
        curTrans = 0
        lastNote = nil
        t = 0.0
        ei = 0
        i = 0
        while (i < cmds.size) 
            cmd = cmds[i]
            c = cmd[0]
            if c == "note" || c == "repeat" 
                if noteIsOn 
                    noteIsOn = false
                    noteLen = t - curNoteStart
                    noteLen *= (1.0 - (curNoteGate.to_f / 10.0))
                    #if (curNoteStart + noteLen) < t 
#                      evs << [curNoteStart + noteLen, (ei+=1), ["noteoff", chan]]
                    lastNote[3] = noteLen if lastNote
                    lastNote = nil
                    #end
                end

                nn = -1
                if c == "repeat" 
                    nn = curNote
                else 

                 nn = cmd[1] + (12 * (curOctave)) + curTrans
                end
                if nn >= 0 && nn <= 127 
                    noteIsOn = true
                    curNote = nn
                    curNoteStart = t
                    curNoteGate = curGate
                    note = ["note", chan, nn, 0] 
                    evs << [t, (ei+=1), note]
                    lastNote = note
                end

                efflen = curLen.to_f
                if cmd[2] > 0
                  efflen = cmd[2].to_f
                end
                slen = (1.0 / efflen) * cmd[3].to_f
                t += slen


            elsif c == "extend" 
                efflen = curLen.to_f
                if cmd[2] > 0
                  efflen = cmd[2].to_f
                end
                slen = (1.0 / efflen) * cmd[3].to_f
                t += slen
            elsif c == "rest" 
                if noteIsOn 
                    noteIsOn = false
                    noteLen = t - curNoteStart
                    noteLen *= (1.0 - (curNoteGate.to_f / 10.0))
                    # evs << [curNoteStart + noteLen, (ei+=1), ["noteoff", chan]]
                    lastNote[3] = noteLen if lastNote
                    lastNote = nil
                end
                
                efflen = curLen.to_f
                if cmd[2] > 0
                  efflen = cmd[2].to_f
                end
                slen = (1.0 / efflen) * cmd[3].to_f
                t += slen
            elsif c == "len" 
                curLen = cmd[1]
            elsif c == "oct" 
                curOctave = cmd[1]
            elsif c == "octup" 
                curOctave += cmd[1]
            elsif c == "octdn" 
                curOctave -= cmd[1]
            elsif c == "transup"
                curTrans = cmd[1]
            elsif c == "transdn"
                curTrans = 0-cmd[1]
            elsif c == "transrelup"
                curTrans += cmd[1]
            elsif c == "transreldn"
                curTrans -= cmd[1]
            elsif c == "tuneup" 
                evs << [t, (ei+=1), ["tune", chan, cmd[1]]]
            elsif c == "tunedn" 
                evs << [t, (ei+=1), ["tune", chan, 0-cmd[1]]]
            elsif c == "duty" 
                evs << [t, (ei+=1), ["duty", chan, cmd[1]]]
            elsif c == "gate" 
                curGate = cmd[1]
            elsif c == "tempo" 
                evs << [t, (ei+=1), ["tempo", cmd[1]]]
            elsif c == "lpstart" 
                (loopI+=1)
                loopStack << [i,1,-1,-1]
            elsif c == "lpend" 
                break if loopI < 0 #invalid
                loopStack[loopI][2] = cmd[1]
                loopStack[loopI][3] = i
                if loopStack[loopI][1] < loopStack[loopI][2] 
                    i = loopStack[loopI][0]
                    loopStack[loopI][1] += 1
                else 
                    loopI -= 1
                    loopStack.pop()
                end
            elsif c == "lpcoda"  
                break if loopI < 0 #invalid
                if loopStack[loopI][2] > -1 && loopStack[loopI][1] >= loopStack[loopI][2] 
                    i = loopStack[loopI][3]
                    loopI -= 1
                    loopStack.pop()
                end
            end
            # ignore other commands
            (i+=1)
        end

        if noteIsOn 
            noteIsOn = false
            noteLen = t - curNoteStart
            noteLen *= (1.0 - (curNoteGate.to_f / 10.0))
            # evs << [curNoteStart + noteLen, (ei+=1), ["noteoff", chan]]
            lastNote[3] = noteLen if lastNote
            lastNote = nil
        end


    end
    evs = evs.map do |e| 
      if e[2][0] == "note"
        [(ppqn.to_f*e[0]).to_i, e[1], [e[2][0], e[2][1], e[2][2], `Math.floor(#{e[2][3]} * #{ppqn.to_f})`]]
      else
        [(ppqn.to_f*e[0]).to_i, e[1], e[2]]
      end
    end
    evs.sort! { |a,b| (a[0]*1000 + a[1]) <=> (b[0]*1000 + b[1]) }
    evo = []
    lt = 0
    evs.each_with_index do |e, i| 
        evo << [e[0]-lt, e[2]]
        lt = e[0]
    end
    evo
  end

# def parse_events(evs) 
#     data = blob(128)
#     evs.each_with_index do |e, i| 
#         data.writen(e[0],'w')
#         ev = e[1]
#         if ev[0] == "noteon" 
#             data.writen(0x90 + ev[1], 'b')
#             data.writen(ev[2], 'b')
#         elsif ev[0] == "noteoff" 
#             data.writen(0x80 + ev[1], 'b')
#         elsif ev[0] == "duty"  
#             data.writen(0xC0 + ev[1], 'b')
#             data.writen(ev[2], 'b')
#         elsif ev[0] == "tune"  
#             data.writen(0xE0 + ev[1], 'b')
#             data.writen(ev[2], 'b')
#         elsif ev[0] == "tempo" 
#             data.writen(0xF3, 'b')
#             data.writen(ev[1], 'w')
#         end
#     end
#     return data
# end

  def parse_mmls(lines)
    @macros = {}
    lines.map do |line|
      cmds = []
      cmds << ["trk", line]
      cmds += parse_mml(line)
      cmds << ["endtrk"]
    end
  end

  def parse s
    parse_mmls s.split('|')
  end

  def channel_count
    @cmds.size
  end
end