class Keys
  def initialize
    @kibo = `new Kibo()`
  end
  def down ary, &block
    `#{@kibo}.down(#{ary}, #{block})`
  end
  def up ary, &block
    `#{@kibo}.up(#{ary}, #{block})`
  end
end