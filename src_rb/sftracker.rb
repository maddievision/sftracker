require 'opal'
require 'browser'
require "opal-jquery"
require "vienna"
require 'lib/common'
require "lib/keys"
require "lib/midi"
require "lib/audio"
require "lib/spc"
require "lib/parser"

class SFTracker

  BUFFER_SIZE = 2048
  CHANNELS = 2

  CONTEXTS = [
    :pattern,
    :note_entry,
    :effect_type_entry,
    :effect_data_entry
  ]

  MIDI_OUT_PORT = 2

  MIDI_KEY_MAP = [
  'z','s','x','d','c','v','g','b','h','n','j','m',
  'q','2','w','3','e','r','5','t','6','y','7','u','i','9','o','0','p'
  ]

  NOTE_NAMES = [
    'C-','C#','D-','D#','E-','F-','F#','G-','G#','A-','A#','B-'
  ]

  COLOR_MAP = [ 112, 3, 113, 115, 112, 3, 113, 115 ]

  COLOR_MAP_TL = [
      [1,2,3],
      [16,32,48],
      [3,56,59],
      [17,42,35],
      [1,2,3],
      [17,42,35],
      [16,32,48],
      [3,56,59]
  ]

    
  def hsv_to_rgb h, s, v
    %x{
      var r, g, b, i, f, p, q, t;
      if (#{h} && #{s} === undefined && #{v} === undefined) {
          #{s} = #{h}.s, #{v} = #{h}.v, #{h} = #{h}.h;
      }
      i = Math.floor(#{h} * 6);
      f = #{h} * 6 - i;
      p = #{v} * (1 - #{s});
      q = #{v} * (1 - f * #{s});
      t = #{v} * (1 - (1 - f) * #{s});
      switch (i % 6) {
          case 0: r = #{v}, g = t, b = p; break;
          case 1: r = q, g = #{v}, b = p; break;
          case 2: r = p, g = #{v}, b = t; break;
          case 3: r = p, g = q, b = #{v}; break;
          case 4: r = t, g = p, b = #{v}; break;
          case 5: r = #{v}, g = p, b = q; break;
      }
      return {
          r: Math.floor(r * 255),
          g: Math.floor(g * 255),
          b: Math.floor(b * 255)
      };
    }
  end


  def hsv2hex(h,s,v)
    rgb = hsv_to_rgb h,s,v
    return `"rgb(" + #{rgb}.r + "," + #{rgb}.g + "," + #{rgb}.b + ")"`
  end

  def round_rect ctx, x, y, width, height, radius, fill, stroke
    %x{
      /**
       * Draws a rounded rectangle using the current state of the canvas. 
       * If you omit the last three params, it will draw a rectangle 
       * outline with a 5 pixel border radius 
       * @param {CanvasRenderingContext2D} ctx
       * @param {Number} x The top left x coordinate
       * @param {Number} y The top left y coordinate 
       * @param {Number} width The width of the rectangle 
       * @param {Number} height The height of the rectangle
       * @param {Number} radius The corner radius. Defaults to 5;
       * @param {Boolean} fill Whether to fill the rectangle. Defaults to false.
       * @param {Boolean} stroke Whether to stroke the rectangle. Defaults to true.
       */
      var ctx = #{ctx};
      var x = #{x};
      var y = #{y};
      var width = #{width};
      var height = #{height};
      var radius = #{radius};
      var fill = #{fill};
      var stroke = #{stroke};
      if (typeof stroke == "undefined" ) {
        stroke = true;
      }
      if (typeof radius === "undefined") {
        radius = 5;
      }
      ctx.beginPath();
      ctx.moveTo(x + radius, y);
      ctx.lineTo(x + width - radius, y);
      ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
      ctx.lineTo(x + width, y + height - radius);
      ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
      ctx.lineTo(x + radius, y + height);
      ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
      ctx.lineTo(x, y + radius);
      ctx.quadraticCurveTo(x, y, x + radius, y);
      ctx.closePath();
      if (stroke) {
        ctx.stroke();
      }
      if (fill) {
        ctx.fill();
      }        
    }
  end

  PALETTE = [
      [0.000, 0.000, 0.125], #00
      [0.608, 0.731, 0.992], #01
      [0.558, 0.576, 1.000], #02
      [0.468, 0.306, 1.000], #03
      [0.396, 0.548, 0.816], #04
      [0.253, 0.478, 0.902], #05
      [0.118, 0.455, 1.000], #06
      [0.097, 0.620, 1.000], #07
      [0.056, 0.643, 1.000], #08
      [0.992, 0.600, 1.000], #09
      [0.959, 0.710, 1.000], #10
      [0.722, 0.506, 1.000], #11
      [0.512, 0.655, 0.647], #12
      [0.033, 0.416, 1.000], #13
      [0.886, 0.612, 1.000], #14
      [0.715, 0.380, 1.000], #15
      [0.833, 0.306, 1.000]  #16
  ]

  #For instrument mapping
  INST_COLOR_MAP = [1,4,8,10,11,12,14,2,5,7,9,3,6,13,15,16,0]


  PROG_KEY_MAP = {
    insert_note_off: ['1'],
    select_all: ['ctrl a'],
    undo: ['ctrl z'],
    :redo => ['ctrl shift z', 'ctrl y'],
    cut: ['ctrl x'],
    copy: ['ctrl c'],
    paste: ['ctrl v'],
    move_left: ['left'],
    move_down: ['down'],
    move_up: ['up'],
    move_right: ['right']
  }

  def run

    @inst_color_map_cache = INST_COLOR_MAP.map do |ind|
      hsv2hex(*PALETTE[ind])
    end

    @midi_processor = lambda do |event|
      handle_midi_event event
    end

    @midi = MIDI.new do |midi|
      midilist = midi.outputs.map do |o| 
        `#{o}.name`
      end.join ", "
      Element["#midilist"].html = midilist
      (0..127).each do |i|
        (0..7).each do |j|
          midi.send(MIDI_OUT_PORT, [0x80 + j, i, 0x00])
        end
      end
      midi.process &@midi_processor
    end

    @spc = SPC.new

    @voice_note_state = [-1,-1,-1,-1,-1,-1,-1,-1]
    @voice_src_state = [-1,-1,-1,-1,-1,-1,-1,-1]


    @visualizer = `new Dotalizer(new DotConductor(),#{@spc.synth})`
    @visual = `document.getElementById('visual')`
    @vctx = `#{@visual}.getContext('2d')`

    each_frame do |t|
      voices = (0..7).map do |i|

        `#{@vctx}.fillStyle = "#000000"`
        `#{@vctx}.fillRect(0, 0, #{@visual}.width, #{@visual}.height)`
        `#{@visualizer}.draw(#{@vctx})`

        vl = @spc.get_dsp_reg 0x01 + (i << 4)
        vr = @spc.get_dsp_reg 0x01 + (i << 4)
        vl -= 0x100 if vl > 0x80
        vr -= 0x100 if vr > 0x80

        a = @spc.get_dsp_reg 0x02 + (i << 4)
        b = @spc.get_dsp_reg 0x03 + (i << 4)
        s = @spc.get_dsp_reg 0x04 + (i << 4)
        v = @spc.get_voice i

        # mn = n - 31
        # mn = 127 if mn > 127
        # mn = 0 if mn < 0
        # st = @voice_note_state[i]
        # ss = @voice_src_state[i]
        # e = `#{v}.env`
        # mn = -1 if e == 0        
        # v = (e / 2000.0) * 127.0
        # v = `Math.ceil(#{v})`
        # v = 0 if v < 0
        # v = 127 if v > 127


        # if mn == -1 && st != mn
        #   @voice_note_state[i] = mn
        #   @voice_src_state[i] = s
        #   @midi.send(MIDI_OUT_PORT, [0x90 + i, st, 0x00])
        # end

        # if mn != -1 && st != mn && st == -1
        #   @midi.send(MIDI_OUT_PORT, [0x90 + i, st, 0x00]) if st > -1
        #   @voice_note_state[i] = mn
        #   @voice_src_state[i] = s
        #   @midi.send(MIDI_OUT_PORT, [0x90 + i, mn, v])
        # end

        "(%02X)%02X%02X %5d %5d %5d" % [s,b,a,`#{v}.env`,vl,vr]

      end.join "\n"

      Element["#test"].html = voices

      nil
    end

    @audio = Audio.new BUFFER_SIZE, CHANNELS
    @audio.process do |e| 
      @spc.processor.call e

      voices = (0..7).each do |i|
        a = @spc.get_dsp_reg 0x02 + (i << 4)
        b = @spc.get_dsp_reg 0x03 + (i << 4)
        s = @spc.get_dsp_reg 0x04 + (i << 4)
        v = @spc.get_voice i

        ff = ((b << 8) + a)
        degrees = 12
        midi_ref = 69
        freq_ref = 440

        n = `(Math.log(#{ff}) - Math.log(#{freq_ref})) / Math.log(2) * #{degrees} + #{midi_ref}`
        mn = n % 8
        st = @voice_note_state[i]
        ss = @voice_src_state[i]
        e = `#{v}.env`
        mn = -1 if e == 0
        v = 0
        if st != mn || ss != s
          if st > -1
            @midi.send(MIDI_OUT_PORT, [0x80, (16 * i) + st, 0x00])
          end

          @voice_note_state[i] = mn
          @voice_src_state[i] = s

          if mn > -1
            @midi.send(MIDI_OUT_PORT, [0x90, (16 * i) + mn, COLOR_MAP[s % 8]])
          end
        end
      end
      nil
    end

    # some browsers suck, and we have to wait a bit before we start
    after 500 do 
      @audio.connect_node
    end

    @keys = Keys.new


    # program

    PROG_KEY_MAP.each do |k, v|
      v.each do |kk|
        @keys.down kk do
          handle_program_event(type: k)
          false
          @key_state[kk.split.last] = true
        end
        @keys.up kk do
          @key_state[kk.split.last] = false
        end
      end 
    end

    # fake midi

    @key_state = {}
    @key_octave = 4

    MIDI_KEY_MAP.each_with_index do |k, i|
      nn = i + (@key_octave * 12)
      @keys.down [k] do
        unless @key_state[k]
          handle_midi_event MIDI.event_from_data [0x90, nn, 127]
          @key_state[k] = true
        end
      end
      @keys.up [k] do
        if @key_state[k]
          handle_midi_event MIDI.event_from_data [0x80, nn, 127]
          @key_state[k] = false
        end
      end
    end


    # @router = Vienna::Router.new

    # @router.route("/load") do |params|

    #   `FS.writeFile("test.spc",#{data})`
    #   @spc.load "test.spc"
    # end
    file_dialog_select = lambda do |evt|
      handle_dialog_file_select evt
    end

    file_local_play = lambda do |evt|
      play_local_file evt
    end

    `document.getElementById('files').addEventListener('change', #{file_dialog_select}, false)`
    `document.getElementById('localload').addEventListener('mouseup',#{file_local_play}, false)`

    @textedit = Element['#textedit']
    @deltas = Element['#deltas']
    @textedit.on("input propertychange") { |evt| update_mml }


    @mmlvis = `document.getElementById('mmlvis')`
    @mvctx = `#{@mmlvis}.getContext('2d')`

    update_mml
  end

  def update_mml
    events = Parser.new(@textedit.value).events
    @deltas.html = events.map { |e| e.inspect }.join "\n"
    t = 0
    ppqn = 96 
    ly = 0
    duty = {}

    shapes = []

    events.each do |e|
      t += e[0]
      ev = e[1]
      if ev[0] == "duty"
        duty[ev[1]] = ev[2]
      elsif ev[0] == "note"
        duty[ev[1]] = 5 unless duty[ev[1]]
        x = (ev[1] * 40)
        y = t 
        w = 30
        h = (ev[3] - 1) 
        ly = y + h + 1
        if h > 0
          sss = ev[2] % 12
          no = `Math.floor(#{ev[2]} / 12.0)`
          shapes << [@inst_color_map_cache[duty[ev[1]] % @inst_color_map_cache.size], x, y, w, h, NOTE_NAMES[sss] + no.to_s]
        end
      end
    end

    db = `Math.ceil(#{ly.to_f} / #{ppqn.to_f})`

    db = 4 if db < 4

    `#{@mmlvis}.height = #{(db) * ppqn.to_f}`

    `#{@mvctx}.fillStyle = "#000000"`
    `#{@mvctx}.fillRect(0, 0, #{@mmlvis}.width, #{@mmlvis}.height)`


    (0..db).each do |i|
      y = i * ppqn 
      x = 0
      w = `#{@mmlvis}.width`
      h = 1

      if i % 4 == 0
        st = '#FFFFFF'
      else
        st = '#888888'
      end

      `#{@mvctx}.strokeStyle=#{st}`
      `#{@mvctx}.fillStyle=#{st}`
      `#{@mvctx}.beginPath()`
      `#{@mvctx}.moveTo(x,y)`
      `#{@mvctx}.lineTo(w,y)`
      `#{@mvctx}.stroke()`


      `#{@mvctx}.font = "12px Lucida Console"`
      tx = "#{i + 1}"
      `#{@mvctx}.fillText(#{tx}, #{w}-30, #{y}+20)`

    end

    shapes.each do |c, x, y, w, h, n|

      #      round_rect ctx, x, y, width, height, radius, fill, stroke
      `#{@mvctx}.fillStyle = #{c}`
      # `#{@mvctx}.fillRect(#{x}, #{y}, #{w}, #{h})`
      round_rect @mvctx, x, y, w, h, 3, true, false

      `#{@mvctx}.fillStyle = "white"`
      `#{@mvctx}.font = "10px Lucida Console"`
      `#{@mvctx}.fillText(#{n}, #{x}+5, #{y}+10)`
    end

    `#{@mmlvis}.scrollTop = #{@mmlvis}.scrollHeight`


  end

  def handle_dialog_file_select evt
    `#{evt}.preventDefault()`
    @load_file = `#{evt}.target.files[0]`
  end

  def play_local_file evt
    @spc.load_file @load_file
  end


  def handle_midi_event e
    if e[:type] != :system #filter system control events
      event_data = e[:data].map { |ed| "%02X" % ed }.join(" ")
      # log "MIDI #{e[:type]} - Channel #{e[:channel]}: #{event_data}"
      @midi.send(MIDI_OUT_PORT, e[:data])
    end
  end

  def handle_program_event e
    # log "Program #{e[:type]}"
  end

  def log text
    Element["#midiLog"] << "<li>#{text}</li>"

    `window.scrollTo(0,document.body.scrollHeight)`
  end
end