require "opal"
require "./sftracker"

Document.ready? do
  SFTracker.new.run
end