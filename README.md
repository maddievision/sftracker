# Setup

Install Ruby 2.1.0

    rvm install 2.1.0

Use it

    rvm use 2.1.0

Install Emscripten

    instructions go here

Install bundle

    bundle

# Build

    rake build

# Run server

    rake server

And view at [http://localhost:9090](http://localhost:9090)