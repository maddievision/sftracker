(function(){


    window.DotConductor = function() {
        this.plugin = {};
        this.settings = {};

        this.register = function() {
            var plugin = this.plugin;
            var settings = this.settings;
            plugin.id = "com.nolimitzone.dotconductor";
            plugin.name = "Dot Conductor";
            plugin.vendor = "No Limit Zone";
            plugin.author = "DJ Bouche";
            plugin.version = 0.5;
            plugin.size = 
                [settings.marginX * 2 + settings.pitchSpacing * 120,
                 settings.marginY * 2 + settings.channelSpacing * 64];
        }

        this.defaults = function() {
            var settings = this.settings;
            //Orientation of dots (0 for horizontal pitch, 1 for vertical pitch)
            settings.orientation = 0;

            //Use inst index for positioning? (0 to use channel, 1 to use inst index)
            settings.rowsByInstIndex = 0;

            settings.pitchSpacing = 9.0;
            settings.channelSpacing = 24.0;

            //What number is regarded as full volume (max 1024)
            settings.mixerVolumeCeiling = 1.0;

            //Input to volume tension bezier curve (0.5 for no curve)
            settings.volumeTension = 0.0;

            //Minimum volume amount when volume is not 0
            settings.volumeOffset = 0.05;

            //Size of dots
            settings.diameter = 21.0;

            //Minimum size of second dot
            settings.minSecondaryDiameter = 3.0;

            //Maximum size of second dot as % of global diameter
            settings.pressureCeiling = 0.9;

            //Divide voice pressure by this much
            settings.pressureRatio = 1.0

            //How much secondary dot opacity is related to primary global opacity
            settings.secondaryOpacityRatio = 1.5;

            //How much does pressure affect global opacity (0.0 no effect - 1.0)
            settings.pressureToOpacity = 0.5;

        }

        //----------------------------------------------------------------------------------------

        //The awesome palette from cap
        this.palette = [
            [0.000, 0.000, 0.125], //00
            [0.608, 0.731, 0.992], //01
            [0.558, 0.576, 1.000], //02
            [0.468, 0.306, 1.000], //03
            [0.396, 0.548, 0.816], //04
            [0.253, 0.478, 0.902], //05
            [0.118, 0.455, 1.000], //06
            [0.097, 0.620, 1.000], //07
            [0.056, 0.643, 1.000], //08
            [0.992, 0.600, 1.000], //09
            [0.959, 0.710, 1.000], //10
            [0.722, 0.506, 1.000], //11
            [0.512, 0.655, 0.647], //12
            [0.033, 0.416, 1.000], //13
            [0.886, 0.612, 1.000], //14
            [0.715, 0.380, 1.000], //15
            [0.833, 0.306, 1.000]  //16
        ];

        //For instrument mapping
        this.colormap = [1,4,8,10,11,12,14,2,5,7,9,3,6,13,15,16,0];

        this.colorWhite = '#FFFFFF';

        this.internal = {};

        this.refresh = function() {
            this.applySpacingSettings();
        }

        this.update = function(display,note) {
            var settings = this.settings;
            var internal = this.internal;
            //cap is a dixel

            display.x = internal.marginX 
                + (note.channel * internal.channelX) 
                + (note.floatingNoteNumber * internal.pitchX)
                + (note.sampleIndex * internal.instX);

            display.y = internal.marginY 
                + (note.channel * internal.channelY) 
                + (note.floatingNoteNumber * internal.pitchY)
                + (note.sampleIndex * internal.instY);


            var cut = 1.0;
            if (note.filterCutoff > 0.0) cut = note.filterCutoff / 255.0;

            //Assign color based on sample index
            var baseColor = this.palette[this.colormap[note.sampleIndex % (this.colormap.length-1)]];
            //saturate based on filter cutoff
            var saturation = baseColor[1] * cut;
            var v = baseColor[2] * (cut);
            display.useHsv(baseColor[0],saturation,v);

            //Assign diameter based on fixed setting
            display.diameter = settings.diameter;

            //Assign volume to opacity
            var vol = (note.volume / settings.mixerVolumeCeiling);


            //Assign second dot diameter to voice pressure
            var mixedPressure = fclip( (note.pressureL / settings.pressureRatio) 
                + (note.pressureR / settings.pressureRatio) / 2.0);
            display.secondDiameter = 
                clip(settings.diameter 
                    * clip(mixedPressure * vol,0.0,settings.pressureCeiling),
                    settings.minSecondaryDiameter,settings.diameter);

            display.opacity = this.alphaFromVolume(vol) 
                * fclip(mixedPressure + 1.0 - (settings.pressureToOpacity));

            //Assign second dot opacity proportion to primary dot opacity
            //and filter cutoff
            display.secondOpacity = fclip(display.opacity * settings.secondaryOpacityRatio) * cut;



            //Assign second dot color to white
            display.useSecondColor(this.colorWhite);

        }

        this.applySpacingSettings = function() {
            var internal = this.internal;
            var settings = this.settings;
            if (settings.orientation == 0) {
                internal.marginX = -200;
                internal.marginY = 50.0;

                //Spacing between pitch on each axis
                internal.pitchX = settings.pitchSpacing;
                internal.pitchY = 0.0;

                if (settings.rowsByInstIndex == 0) {
                    //Spacing between channels on each axis
                    internal.channelX = 0.0;
                    internal.channelY = settings.channelSpacing;
                    internal.instX = 0.0;
                    internal.instY = 0.0;
                }
                else {
                    //Spacing between channels on each axis
                    internal.channelX = 0.0;
                    internal.channelY = 0.0;
                    internal.instX = 0.0;
                    internal.instY = settings.channelSpacing;
                }

            }
            else if (settings.orientation == 1) {
                internal.marginX = 50.0;
                internal.marginY = 900.0;

                //Spacing between pitch on each axis
                internal.pitchX = 0.0;
                internal.pitchY = -1 * settings.pitchSpacing;

                if (settings.rowsByInstIndex == 0) {
                    //Spacing between channels on each axis
                    internal.channelX = settings.channelSpacing;
                    internal.channelY = 0.0;
                    internal.instX = 0.0;
                    internal.instY = 0.0;
                }
                else {
                    //Spacing between channels on each axis
                    internal.channelX = 0.0;
                    internal.channelY = 0.0;
                    internal.instX = settings.channelSpacing;
                    internal.instY = 0.0;
                }    
            }
        }




        //Apply bezier curve tension
        this.alphaFromVolume = function(v) {
            var settings = this.settings;
            var alpha = fclip(v);
            alpha = simplebez(alpha,settings.volumeTension);
            if (alpha > 0.0) alpha += settings.volumeOffset;
            return fclip(alpha);
        }
            //Clip value to min/max
        function clip(v,min,max) {
            if (v > max) return max;
            else if (v < min) return min;
            return v;
        }

        //Clip to 0.0 - 1.0
        function fclip(v) {
            return clip(v,0.0,1.0); 
        }

        //Crappy bezier thing
        function simplebez(x,z) {
            return 2.0 * (1.0 - x) * x * (1.0 - z) + (x * x);
        }

    };

})();