(function(){

function HSVtoRGB(h, s, v) {
    var r, g, b, i, f, p, q, t;
    if (h && s === undefined && v === undefined) {
        s = h.s, v = h.v, h = h.h;
    }
    i = Math.floor(h * 6);
    f = h * 6 - i;
    p = v * (1 - s);
    q = v * (1 - f * s);
    t = v * (1 - (1 - f) * s);
    switch (i % 6) {
        case 0: r = v, g = t, b = p; break;
        case 1: r = q, g = v, b = p; break;
        case 2: r = p, g = v, b = t; break;
        case 3: r = p, g = q, b = v; break;
        case 4: r = t, g = p, b = v; break;
        case 5: r = v, g = p, b = q; break;
    }
    return {
        r: Math.floor(r * 255),
        g: Math.floor(g * 255),
        b: Math.floor(b * 255)
    };
}


function hsv2hex(h,s,v) {
  var rgb = HSVtoRGB(h,s,v);
  return "rgb(" + rgb.r + "," + rgb.g + "," + rgb.b + ")";
}

window.DotDisplay = function() {
  this.x = 0;
  this.y = 0;
  this.diameter = 0;
  this.secondDiameter = 0;
  this.opacity = 0;
  this.secondOpacity = 0;

  this.color = "#000000";
  this.secondColor = "#000000";

  this.useHsv = function(h,s,v) {
    this.color = hsv2hex(h,s,v);
  };
  this.useColor = function(hex) {
    this.color = hex;
  };
  this.useSecondHsv = function(h,s,v) {
    this.secondColor = hsv2hex(h,s,v);
  };
  this.useSecondColor = function(hex) {
    this.secondColor = hex;    
  };
};

window.DotNote = function() {
  this.channel = 0;
  this.floatingNoteNumber = 0;
  this.sampleIndex = 0;
  this.pressureL = 0;
  this.pressureR = 0;
  this.filterCutoff = 255;
  this.volume = 0;
};

function drawcircle(ctx,x,y,rad,c,a) {
  ctx.beginPath();
  ctx.arc(x,y,rad,0,2*Math.PI,false);
  ctx.closePath();
  ctx.globalAlpha = a;
  ctx.fillStyle = c;
  ctx.fill();
  ctx.strokeStyle = c;
  ctx.stroke();
}

window.Dotalizer = function(plugin,synth) {
  this.plugin = plugin;
  this.synth = synth;

  plugin.defaults();
  plugin.register();
  plugin.refresh();


  this.draw = function(ctx) {
    var synth = this.synth;
    var plugin = this.plugin;
    var display = new DotDisplay();
    var note = new DotNote();

    var voicecount = 8;

//    proc.background(0);

    for (var i = 0; i < voicecount; i++) {
      var vce = synth.get_voice(i);
      if (vce.enabled) {
        note.channel = i;
        note.floatingNoteNumber = vce.note;
        note.sampleIndex = vce.srcn;
        var p = vce.prs;
        note.pressureL = p[0];
        note.pressureR = p[1];
        note.volume = vce.avol;

        plugin.update(display,note);

        //draw
        var x = display.x;
        var y = display.y;
        var r1 = display.diameter / 2.0;
        var r2 = display.secondDiameter / 2.0;

        var c1 = display.color;
        var c2 = display.secondColor;
        drawcircle(ctx,x,y,r1,c1,display.opacity);
        drawcircle(ctx,x,y,r2,c2,display.secondOpacity);
      }

    }

  }
}

})();
